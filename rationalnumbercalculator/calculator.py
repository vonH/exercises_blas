import math

class RationalNumber():
    """
    This class/object will create and represent a rational number, on initiation it checks if denomitor == 0 and raises an error if so.
    After that on initialization it will also reduce the rational number to the lowest possible value, but finding the 'Greatest common denominator'
    and devide the numerator and denominator with this value.

    The class can then be used to add, subtract, multiply or devide two rational numbers using the operators '+' '-' '*' and '/'
    """

    def __init__(self, a, b):
        if b == 0:
            raise TypeError('Denominator cant b "0", aka the 2nd argument')
        self.numerator = a
        self.denominator = b
        self._reduce_rationalnumber()

    @property
    def number(self):
        return f'{self.numerator}/{self.denominator}'

    #Question here: Is it also convention to name a method _* if you only intend it to be used internally, i think i've read that some place.
    def _reduce_rationalnumber(self):
        gcd = math.gcd(self.numerator, self.denominator)
        self.numerator = int(self.numerator / gcd)
        self.denominator = int(self.denominator / gcd)

    def __add__(self, right_number):
        new_numerator = self.numerator * right_number.denominator + right_number.numerator * self.denominator
        new_denominator = self.denominator * right_number.denominator
        return RationalNumber(new_numerator, new_denominator)

    def __sub__(self, right_number):
        new_numerator = self.numerator * right_number.denominator - right_number.numerator * self.denominator
        new_denominator = self.denominator * right_number.denominator
        return RationalNumber(new_numerator, new_denominator)

    def __mul__(self, right_number):
        new_numerator = self.numerator * right_number.numerator 
        new_denominator = self.denominator * right_number.denominator
        return RationalNumber(new_numerator, new_denominator)

    def __truediv__(self, right_number):
        new_numerator = self.numerator * right_number.denominator 
        new_denominator = self.denominator * right_number.numerator
        return RationalNumber(new_numerator, new_denominator)
   

if __name__ == '__main__':
    pass