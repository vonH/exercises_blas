from calculator import RationalNumber
import unittest


class CalcInitTestCase(unittest.TestCase):
 
    def test_zero_denominator(self):
        with self.assertRaises(TypeError):
            RationalNumber(5, 0)

    def test_postive_numbers(self):
        num = RationalNumber(4,9)
        self.assertEqual(num.number, "4/9")
        self.assertEqual(num.numerator, 4)
        self.assertEqual(num.denominator, 9)

    def test_negative_numerator(self):
        num = RationalNumber(-4,9)
        self.assertEqual(num.number, "-4/9")
        self.assertEqual(num.numerator, -4)
        self.assertEqual(num.denominator, 9)

    def test_negative_denominator(self):
        num = RationalNumber(4,-9)
        self.assertEqual(num.number, "4/-9")
        self.assertEqual(num.numerator, 4)
        self.assertEqual(num.denominator, -9)

    def test_negative_numbers(self):
        num = RationalNumber(-4,-9)
        self.assertEqual(num.number, "-4/-9")
        self.assertEqual(num.numerator, -4)
        self.assertEqual(num.denominator, -9)


class CalcAddTestCase(unittest.TestCase):
   
    def test_add_two_positives(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(1, 4)
        add = num1 + num2
        self.assertEqual(add.number, "3/4")

    def test_add_two_negatives(self):
        num1 = RationalNumber(-4, -8)
        num2 = RationalNumber(-1, -4)
        add = num1 + num2
        self.assertEqual(add.number, "3/4")

    def test_add_two_neg_numerators(self):
        num1 = RationalNumber(-4, 8)
        num2 = RationalNumber(-1, 4)
        add = num1 + num2
        self.assertEqual(add.number, "-3/4")

    def test_add_pos_neg(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(-1, 4)
        add = num1 + num2
        self.assertEqual(add.number, "1/4")
        

class CalcSubTestCase(unittest.TestCase):
    
    def test_sub_two_positives(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(1, 4)
        sub = num1 - num2
        self.assertEqual(sub.number, "1/4")

    def test_sub_two_negatives(self):
        num1 = RationalNumber(-4, -8)
        num2 = RationalNumber(-1, -4)
        sub = num1 - num2
        self.assertEqual(sub.number, "1/4")

    def test_sub_two_neg_numerators(self):
        num1 = RationalNumber(-4, 8)
        num2 = RationalNumber(-1, 4)
        sub = num1 - num2
        self.assertEqual(sub.number, "-1/4")

    def test_sub_pos_neg(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(-1, 4)
        sub = num1 - num2
        self.assertEqual(sub.number, "3/4")


class CalcMulTestCase(unittest.TestCase):

    def test_mul_two_positives(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(1, 4)
        mul = num1 * num2
        self.assertEqual(mul.number, "1/8")

    def test_mul_two_negatives(self):
        num1 = RationalNumber(-4, -8)
        num2 = RationalNumber(-1, -4)
        mul = num1 * num2
        self.assertEqual(mul.number, "1/8")

    def test_mul_two_neg_numerators(self):
        num1 = RationalNumber(-4, 8)
        num2 = RationalNumber(-1, 4)
        mul = num1 * num2
        self.assertEqual(mul.number, "1/8")

    def test_mul_pos_neg(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(-1, 4)
        mul = num1 * num2
        self.assertEqual(mul.number, "-1/8")

class CalcTruedivTestCase(unittest.TestCase):

    def test_div_two_positives(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(1, 4)
        div = num1 / num2
        self.assertEqual(div.number, "2/1")

    def test_div_two_negatives(self):
        num1 = RationalNumber(-4, -8)
        num2 = RationalNumber(-1, -4)
        div = num1 / num2
        self.assertEqual(div.number, "2/1")

    def test_div_two_neg_numerators(self):
        num1 = RationalNumber(-4, 8)
        num2 = RationalNumber(-1, 4)
        div = num1 / num2
        self.assertEqual(div.number, "-2/-1")

    def test_div_pos_neg(self):
        num1 = RationalNumber(4, 8)
        num2 = RationalNumber(-1, 4)
        div = num1 / num2
        self.assertEqual(div.number, "2/-1")